/**
 * Created with IntelliJ IDEA.
 Venu K Tangirala
 */

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapreduce.lib.jobcontrol.ControlledJob;
import org.apache.hadoop.mapreduce.lib.jobcontrol.JobControl;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.thirdparty.guava.common.collect.Iterators;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.util.Date;
import java.util.Formatter;
import java.util.Iterator;

public class Main implements Tool{

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(),new Main(),args);
        System.exit(res);
    }

    @Override
    public int run(String[] strings) throws Exception {

        Configuration configuration1 = new Configuration();
        Configuration configuration2 = new Configuration();

        Job job1 =new Job(configuration1,"chain1");
//        job1.setJobName("chain1");

        job1.setInputFormatClass(TextInputFormat.class);
        job1.setOutputFormatClass(TextOutputFormat.class);

        job1.setMapOutputKeyClass(Text.class);
        job1.setOutputValueClass(IntWritable.class);

        job1.setMapperClass(WordMapper.class);
        job1.setCombinerClass(WordReducer.class);
        job1.setReducerClass(WordReducer.class);

        FileInputFormat.setInputPaths(job1, new Path("hdfs://localhost/data/nohup"));

        Formatter formatter = new Formatter();
        String outputDir = "hdfs://localhost/output/"
                +formatter.format("%1$tm%1$td%1$tH%1$tM%1$tS",new Date());

        FileOutputFormat.setOutputPath(job1,new Path(outputDir));

        Job job2 =new Job();
        job2.setJobName("chain2");

        job2.setInputFormatClass(TextInputFormat.class);
        job2.setOutputFormatClass(TextOutputFormat.class);

        job2.setMapperClass(Mapper2.class);
        job2.setCombinerClass(Reducer2.class);
        job2.setReducerClass(Reducer2.class);

        JobControl jobControl = new JobControl("jobControl");
        jobControl.addJob(new ControlledJob(configuration1));
        jobControl.addJob(new ControlledJob(configuration2));
//
        return job1.waitForCompletion(true)? 0:1;
    }

    @Override
    public void setConf(Configuration entries) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Configuration getConf() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}

class WordMapper extends org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,Text,IntWritable>{
    protected void map(LongWritable keyIn , Text valueIn , Context context) throws IOException, InterruptedException {
        String line = valueIn.toString();
        String[] word = line.split(" ");

        Iterator<String> iterator = Iterators.forArray(word);
        while (iterator.hasNext()){
            context.write(new Text(iterator.next().toString()), new IntWritable(1));

        }
    }

}

class WordReducer extends Reducer<Text,IntWritable,Text,IntWritable>{

    protected void reduce(Text keyIn, Iterable<IntWritable> valueIn, Context context) throws IOException, InterruptedException {

        int numOfWords =0;

        for(IntWritable values: valueIn){
            numOfWords+=values.get();
        }
        context.write(keyIn,new IntWritable(numOfWords));
    }


}

class Mapper2 extends org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,Text,IntWritable>{
    protected void map(LongWritable keyIn , Text valueIn , Context context) throws IOException, InterruptedException {
        String line = valueIn.toString();
        line.replace("u'","").replace("'","").replace("[","").replace("]","").replace("{","").replace("}","");
        line.replaceAll("[0-9]","");
        line.toUpperCase();
        String[] word = line.split(" ");

        Iterator<String> iterator = Iterators.forArray(word);
        while (iterator.hasNext()){
            context.write(new Text(iterator.next().toString()), new IntWritable(1));

        }
    }

}

class Reducer2 extends Reducer<Text,IntWritable,Text,IntWritable>{

    protected void reduce(Text keyIn, Iterable<IntWritable> valueIn, Context context) throws IOException, InterruptedException {

        int numOfWords =0;

        for(IntWritable values: valueIn){
            numOfWords+=values.get();
        }
        context.write(keyIn,new IntWritable(numOfWords));
    }


}